﻿using System.IO;
using System.Threading.Tasks;

namespace s3fileuploader
{
    public interface IFileService
    {
        Task<bool> IsDirectoryExist(string bucketName, string filePath);
        Task<string> UploadFileAsync(string bucketName, string filePath, FileInfo fileInfo, Stream fileToUpload);
    }
}
