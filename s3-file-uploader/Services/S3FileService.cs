﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace s3fileuploader.Services
{
    public class S3FileService : IFileService
    {
        //private const string bucketName = "*** provide bucket name ***";
        //private const string keyName = "*** provide a name for the uploaded object ***";
        //private const string filePath = "*** provide the full path name of the file to upload ***";
        // Specify your bucket region (an example region is shown).
        private readonly RegionEndpoint bucketRegion = RegionEndpoint.USWest2;
        private IAmazonS3 s3Client;
        private const string awsAccessKeyId = "AKIAJK66GB7LPGYDKV4A";
        private const string awsSecretAccessKey = "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";

        public S3FileService(RegionEndpoint bucketRegion=null)
        {
            this.bucketRegion = bucketRegion == null ? RegionEndpoint.APSoutheast1 : bucketRegion;
            s3Client = new AmazonS3Client(awsAccessKeyId, awsSecretAccessKey, this.bucketRegion);
        }

        private static string fileNameToObjectKey(string filePath, string filename)
        {
            return filePath.EndsWith("/") ? $"{filePath}{filename}" : $"{filePath}/{filename}";
        }

        public async Task<bool> IsDirectoryExist(string bucketName, string filePath)
        {
            try
            {
                var isBucketExist = await s3Client.DoesS3BucketExistAsync(bucketName);
                if(isBucketExist)
                {
                    ListObjectsV2Request request = new ListObjectsV2Request
                    {
                        BucketName = bucketName,
                        Prefix = filePath,
                        MaxKeys = 1
                    };
                    var response = await s3Client.ListObjectsV2Async(request);
                    var isFilePathExist = response.S3Objects.Any();
                    return isBucketExist && isFilePathExist;
                }
                return false;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                Console.WriteLine("S3 error occurred. Exception: " + amazonS3Exception.ToString());
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.ToString());
                Console.ReadKey();
            }
            return false;
        }

        public async Task<string> UploadFileAsync(string bucketName, string filePath, FileInfo fileInfo, Stream fileToUpload)
        {
            var objectPath = string.Empty;
            try
            {
                var fileTransferUtility = new TransferUtility(s3Client);

                // Option 3. Upload data from a type of System.IO.Stream.
                var filename = $"{Path.GetRandomFileName()}{fileInfo.Extension}";
                var objectKey = fileNameToObjectKey(filePath, filename);
                
                    await fileTransferUtility.UploadAsync(fileToUpload, bucketName, objectKey);
                

                objectPath = $"{bucketName}/{objectKey}";
                // set cdnPath;
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered on server. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }

            return objectPath;
        }
    }
}

