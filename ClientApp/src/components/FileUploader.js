﻿import React, { Component } from 'react';
import Dropzone from 'react-dropzone-uploader';
import 'react-dropzone-uploader/dist/styles.css';

export class FileUploader extends Component {
    static displayName = FileUploader.name;

    //constructor(props) {
    //    super(props);
    //    this.state = { currentCount: 0 };
    //    this.incrementCounter = this.incrementCounter.bind(this);
    //}
    constructor(props) {
        super(props);
        this.state = { appName: props.match.params.appName };
        this.getUploadParams = this.getUploadParams.bind(this);
        this.handleChangeStatus = this.handleChangeStatus.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    //incrementCounter() {
    //    this.setState({
    //        currentCount: this.state.currentCount + 1
    //    });
    //}
    // specify upload params and url for your files
    getUploadParams({ meta }) {
        //return { url: 'https://httpbin.org/post' }
        return { url: 'api/v1/file?appName=' + this.state.appName }
    }

    // called every time a file's `status` changes
    handleChangeStatus({ meta, file }, status) {
        console.log(status, meta, file)
    }

    // receives array of files that are done uploading when submit button is clicked
    handleSubmit(files, allFiles) {
        console.log(files.map(f => f.meta))
        allFiles.forEach(f => f.remove())
    }

    render() {
        return (
            <div>
                <h1>{this.state.appName}</h1>
                <Dropzone
                    getUploadParams={this.getUploadParams}
                    onChangeStatus={this.handleChangeStatus}
                    onSubmit={this.handleSubmit}
                    accept="image/*,audio/*,video/*,application/pdf"
                    />
            </div>
        );
    }
}

