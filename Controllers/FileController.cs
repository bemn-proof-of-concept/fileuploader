﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using s3fileuploader;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace react_file_uploader.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class FileController : ControllerBase
    {
        private IWebHostEnvironment _hostingEnvironment;
        private IConfiguration _configuration;
        private IFileService _fileService;
        private readonly string _bucketName;

        public FileController(IWebHostEnvironment environment, IFileService fileService, IConfiguration configuration)
        {
            _hostingEnvironment = environment;
            _fileService = fileService;
            _configuration = configuration;
            _bucketName = _configuration["App:Bucket"];
            // _bucketName = "haymarket-asia-uat";
        }
        // GET: /<controller>/
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return Ok("OK.");
        }

        [HttpPost("")]
        public async Task<IActionResult> Upload([FromQuery] string appName, List<IFormFile> file)
		{
            //long size = file.Sum(f => f.Length);
            List<string> filePaths = new List<string>{ };

            foreach (var formFile in file)
            {
                if (formFile.Length > 0)
                {
                    var fileInfo = new FileInfo(formFile.FileName);
                    //var filePath = Path.Combine(_hostingEnvironment.ContentRootPath, "upload-buffer", $"{Path.GetRandomFileName()}{fileInfo.Extension}");
                    using (var stream = new MemoryStream())
                    {
                        var filePath = $"{appName}/{_configuration["App:ContentPath"]}";
                        if(await _fileService.IsDirectoryExist(_bucketName, filePath))
                        {
                            await formFile.CopyToAsync(stream);
                            var objectPath = await _fileService.UploadFileAsync(_bucketName, filePath, fileInfo, stream);
                            //await formFile.CopyToAsync(stream);
                            filePaths.Add(objectPath);
                        }
                    }
                }
            }

            // Process uploaded files
            // Don't rely on or trust the FileName property without validation.

            return Ok(new { count = file.Count, filePaths });
        }
    }
}
